import{ HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Todo } from 'src/app/models/todo.models';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit{

  todos:Todo[] = [];
  filteredToDos:Todo[] = [];

  inputTodo:string = "";
  constructor(private http: HttpClient){}

  ngOnInit(): void {
    this.fetchdata()
    this.todos = [    ]
  }
  fetchdata(){
    this.http.get<Todo[]>('https://jsonplaceholder.typicode.com/todos')
    .subscribe((data:Todo[]) => {
      this.todos = data.filter(todo => todo.completed === false && todo.userId === 1)
      console.log(this.todos)
    })
  }

  toggleDone (id:number) {
    this.todos.map((v, i) => {
      if (i == id) v.completed = !v.completed;

      return v;
    })
  }

  deleteTodo (id:number) {
    this.todos = this.todos.filter((v, i) => i !== id);
  }

  addTodo () {
    this.todos.push({
     userId: 1,
     id: this.todos.length+1,
     title: this.inputTodo,
     completed: false
    });

    this.inputTodo = "";
    console.log(this.todos);
  }
}
